import React, { Component } from 'react'
class form extends Component {
    constructor(){
        super();
        this.state={
          form:{
            userName:"",
            dob:"",
            emailId:"",
            state:"",
            gender:"",
            password:""
            
          },
          valid:false,
          formDetailsShow:false
        }
      }
      getInputValues=(event)=>{
        let {form}=this.state;
        console.log(form)
        let name=event.target.name;
        let value=event.target.value;
        // console.log(name,value,form,{...form,[name]:value})
        this.setState({form:{...form,[name]:value}},()=>{
          if(this.state.form.userName && this.state.form.gender && this.state.form.dob && this.state.form.emailId && this.state.form.password && this.state.form.state){
            this.setState({valid:true})
          }
        });

    
      }
      registerForm=(event)=>{
        event.preventDefault();
        this.setState({formDetailsShow:true})
        
    
      }
    
      render(){
        console.log(this.state.form)
        return (
          <div className="container">
            <div className="row">
              <div className="col-6 offset-3">
    
                <br/>
                  <div className="card">
                    <div className="card-header text-center">
                      <h2>Registration Form</h2> 
    
                    </div>
                    <div className="card-body">
                    <form onSubmit={this.registerForm}>
                                <div className="form-group">
                                    <label htmlFor="userName">Username<sup className="text-danger sup">*</sup>:</label>
                                    <input name="userName" id="userName" type="text" className="form-control" onChange={this.getInputValues} />
                                    <div className="text-danger" id="userNameError"></div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="emailId">EmailId<sup className="text-danger sup">*</sup>:</label>
                                    <input name="emailId" id="emailId" type="text" className="form-control"  onChange={this.getInputValues}/>
                                    <div className="text-danger" id="emailIdError"></div>
                                </div>
                               
                                {/* <div className="form-group">
                                <div className="form-check form-check-inline">
                                    <label className="form-check-label">
                                      <input type="radio" name="gender" value="male" className="form-check-input" onChange={this.getInputValues}></input>Male
                                    </label> </div>
                                <div className="form-check form-check-inline">
                                <label className="form-check-label">
                                      <input type="radio" name="gender" value="female" className="form-check-input" onChange={this.getInputValues}></input>Female
                                    </label>
                               
                                </div>
                                </div>
    
                                <div className="form-group">
                                  <label htmlFor="state">State<sup className="text-danger sup">*</sup>:</label>
                                  <select onChange={this.getInputValues} name="state" className="form-control">
                                    <option value="" hidden >--Select--</option>
                                    <option value="tamilnadu">TamilNadu</option>
                                    <option value="karnataka" >Karnataka</option>
                                    <option value="kerala" >Kerala</option>
                                  </select>
    
                                </div>
                               
                                <div className="form-group">
                                    <label htmlFor="dob">DOB<sup className="text-danger sup">*</sup>:</label>
                                    <input name="dob" id="dob" type="date" className="form-control"  onChange={this.getInputValues}/>
                                    <div className="text-danger" id="dobError"></div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="password">Password<sup className="text-danger sup">*</sup>:</label>
                                    <input name="password" id="password" type="password" className="form-control"  onChange={this.getInputValues}/>
                                    <div className="text-danger" id="passwordError"></div>
                                </div> */}
                                <button type="submit" id="submitButton" className="btn btn-primary" disabled={!this.state.valid}>Submit</button>
                                <div></div>
                                {/* {this.state.form.name} */}
                                 {/* <a href="./updatePage.html"><button type="button" className="btn btn-warning" >Update Details</button></a>  */}
                            </form>
        {this.state.formDetailsShow?<>
        <div>Name:{this.state.form.userName}</div><div>Email-id:{this.state.form.emailId}</div>
        <div>DOB:{this.state.form.dob}</div><div>Gender:{this.state.form.gender}</div><div>State:{this.state.form.state}</div></>:null}
    
                    </div>
    
                  </div>
    
                </div> 
              
              
              </div> 
    
          </div>
        )
      }
    
    
}

export default form;