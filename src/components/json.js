import React, { Component } from 'react';
import axios from "axios";
class json extends Component {
    constructor(){
        super();
        this.state={
            data:""
        }
    }

    componentDidMount=()=>{
        axios.get("https://jsonplaceholder.typicode.com/todos/1").then((response)=>{
            this.setState({data:response.data.title})

        }).catch((error)=>{
            if(error.status){
                this.setState({data:"No Data Found"})
            }
            else{
                this.setState({data:"Server Error"})
            }
        })
    }
    render() {
        return (
            <>
            <br/><br/>
            <h2 className="text-center">{this.state.data}</h2>
            </>
        );
    }
}

export default json;